import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'headerLoginWidget.dart'; // Import the flutter_secure_storage package

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  String? _message = 'لطفا نام کاربری و رمز عبور را وارد نمایید';

  Future<void> login() async {
    String username = _usernameController.text;
    String password = _passwordController.text;

    setState(() {
      _message = 'لطفا صبر کنید';
    });
    // Make API request
    String apiUrl =
        'https://application.shahinvest.ir/wp-json/jwt-auth/v1/token';
    Map<String, String> headers = {'Content-Type': 'application/json'};
    String body = '{"username": "$username", "password": "$password"}';

    http.Response response =
        await http.post(Uri.parse(apiUrl), headers: headers, body: body);
    Map<String, dynamic> responseBody = jsonDecode(response.body);

    if (response.statusCode == 200) {
      String token = responseBody['token'];
      String userEmail = responseBody['user_email'];
      String userNicename = responseBody['user_nicename'];
      String userDisplayName = responseBody['user_display_name'];
      const storage = FlutterSecureStorage();
      await storage.write(key: 'token', value: token);
      await storage.write(key: 'user_email', value: userEmail);
      await storage.write(key: 'user_nicename', value: userNicename);
      await storage.write(key: 'user_display_name', value: userDisplayName);

      // Navigate to home page
      if (context.mounted) {
        Navigator.of(context)
          .pushNamedAndRemoveUntil('/homePage', (Route<dynamic> route) => false);
      }
    } else {
      setState(() {
        _message = 'نام کاربری یا رمز اشتباه میباشد';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 240,
                child: HeaderWidget(240, true, Icons.login_rounded),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                      child: null,
                    ),
                    TextField(
                      controller: _usernameController,
                      decoration: const InputDecoration(
                        hintText: 'نام کاربری',
                      ),
                      textAlign: TextAlign.right,
                    ),
                    const SizedBox(
                      height: 20,
                      child: null,
                    ),
                    TextField(
                      controller: _passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                        hintText: 'رمز عبور',
                      ),
                      textAlign: TextAlign.right,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(_message!),
                    const SizedBox(
                      height: 15,
                    ),
                    TextButton(
                        onPressed: () =>
                            Navigator.of(context).pushNamed('/loginPageOtp'),
                        child: const Text('بازنشانی رمز عبور')),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      onPressed: login,
                      child: const Text('ورود'),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
