import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import '../main.dart';
import 'headerLoginWidget.dart'; // Import the flutter_secure_storage package

class LoginPageOtp extends StatefulWidget {
  const LoginPageOtp({Key? key}) : super(key: key);

  @override
  _LoginPageOtpState createState() => _LoginPageOtpState();
}

class _LoginPageOtpState extends State<LoginPageOtp> {
  final TextEditingController _shomareMobileController =
      TextEditingController();
  final TextEditingController _otpController = TextEditingController();

  String? _message = 'لطفا شماره موبایل خود را وارد نمایید';
  bool _isSending = false;
  bool _isSmsSend = false;

  Future<void> sendOtpRequest() async {
    String shomareMobile = _shomareMobileController.text;

    makeRequestOtp(shomareMobile, context) async {
      String apiUrl =
          'https://application.shahinvest.ir/wp-json/send_sms_verification';
      http.Response response = await http.post(Uri.parse(apiUrl), body: {
        'shomareMobile': shomareMobile!,
      });

      if (response.statusCode == 200) {
        setState(() {
          _message = jsonDecode(response.body);
          if (jsonDecode(response.body) == 'اس ام اس با موفقیت ارسال شد') {
            _isSmsSend = true;
          }
          _isSending = false;
        });
      } else {
        setState(() {
          _message = 'عملیات نا موفق';
        });
      }
    }

    setState(() {
      if (null != checkPhonePatern(_shomareMobileController.text)) {
        _message = checkPhonePatern(_shomareMobileController.text);
      } else {
        _message = 'لطفا صبر کنید';
        _isSending = true;
        makeRequestOtp(shomareMobile, context);
      }
    });

    // Make API request
  }

  Future<void> sendPasswordResetRequest() async {
    String otpCode = _otpController.text;
    String shomareMobile = _shomareMobileController.text;

    makePasswordReset(otpCode, context) async {
      String apiUrl =
          'https://application.shahinvest.ir/wp-json/reset_password_request';
      http.Response response = await http.post(Uri.parse(apiUrl), body: {
        'otpCode': otpCode!,
        'shomareMobile': shomareMobile!,
      });
      print(response.body);
      if (response.statusCode == 200) {
        setState(() {
          _message = jsonDecode(response.body);
          if (jsonDecode(response.body) == 'رمز عبور بازنشانی شد و به زودی برای شما ارسال خواهد شد.') {
            _isSmsSend = true;
            if (context.mounted) {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil('/homePage', (Route<dynamic> route) => false);
            }
          }
          _isSending = false;
        });

      } else {
        setState(() {
          _message = 'عملیات نا موفق';
        });
      }
    }

    setState(() {
      if (null != checkPhonePatern(_shomareMobileController.text)) {
        _message = checkPhonePatern(_shomareMobileController.text);
      } else {
        _message = 'لطفا صبر کنید';
        _isSending = true;
        makePasswordReset(otpCode,context);
      }
    });

    // Make API request
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Form(
              child: Column(
            children: [
              const SizedBox(
                height: 240,
                child: HeaderWidget(240, true, Icons.login_rounded),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 40,
                      child: null,
                    ),
                    Visibility(
                        visible: !_isSmsSend,
                        child: TextFormField(
                          validator: (val) => checkPhonePatern(val!),
                          controller: _shomareMobileController,
                          decoration: const InputDecoration(
                            hintText: 'شماره موبایل',
                          ),
                          textAlign: TextAlign.right,
                        )),
                    const SizedBox(
                      height: 20,
                      child: null,
                    ),
                    Visibility(
                      visible: _isSmsSend,
                      child: TextFormField(
                        maxLength: 5,
                        controller: _otpController,
                        decoration: const InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey,
                              width: 2.0,
                            ),
                          ),
                          hintText: 'کد یکبار مصرف را وارد نمایید',
                        ),

                      ),
                    ),
                    const SizedBox(
                      height: 45,
                    ),
                    Text(_message!),
                    const SizedBox(
                      height: 15,
                    ),
                    TextButton(
                        onPressed: () =>
                            Navigator.of(context).pushNamed('/loginPage'),
                        child: const Text('ورود با نام کاربری و کلمه عبور')),
                    const SizedBox(
                      height: 15,
                    ),
                    Visibility(
                        visible: !_isSmsSend,
                        child: ElevatedButton(
                          onPressed: sendOtpRequest,
                          child: const Text('ارسال کد یکبار مصرف'),
                        )),
                    Visibility(
                        visible: _isSmsSend,
                        child: ElevatedButton(
                          onPressed: sendPasswordResetRequest,
                          child: const Text('بازنشانی رمز عبور'),
                        )),
                  ],
                ),
              )
            ],
          )),
        ),
      ),
    );
  }
}

checkPhonePatern(String phoneNumber) {
  RegExp phoneRegex =
      RegExp("^(?:(?:(?:\\+?|00)(98))|(0))?((?:90|91|92|93|99)[0-9]{8})");
  if (phoneRegex.hasMatch(phoneNumber)) {
    return null;
  } else {
    return 'لطفا فرمت شماره موبایل را بررسی کنید';
  }
}
