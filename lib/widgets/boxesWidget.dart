import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget buildRowWithOneBox(String icon, String text, context, anchorTo) {
  return Container(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildBox(icon, text, context, anchorTo),
      ],
    ),
  );
}

Widget buildBoxes(String icon, String text,BuildContext context,String anchorTo) {
  return Container(
    decoration: BoxDecoration(boxShadow: [
      BoxShadow(
        color: Colors.white.withOpacity(0.05),
        spreadRadius: 7,
        blurRadius: 50,
        offset: const Offset(0, 33), // changes position of shadow
      ),
    ]),
    child: buildBox(icon, text, context, anchorTo),
  );
}

Widget buildBox(
    String icon, String text, BuildContext context, String anchorTo) {
  return GestureDetector(
    onTap: () {
      Navigator.pushNamed(context, '/$anchorTo');
    },
    child: Container(
      width: 150,
      height: 140,
      margin: const EdgeInsets.all(10),
      color: Colors.grey.shade100,
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 15),
            width: 80,
            child: Image.asset(icon),
          ),
          const Divider(color: Colors.white, thickness: 2),
          Text(text, style: const TextStyle(fontSize: 12)),
        ],
      ),
    ),
  );
}
