import 'package:flutter/material.dart';

imageWithTextWidget(context,  String icon,String text) {
  return Container(
    margin: const EdgeInsets.only(bottom: 10),
    color: Colors.white,
    width: MediaQuery.of(context).size.width,
    child: Column(
      children: [
        Image.asset(icon, width: MediaQuery.of(context).size.width * 0.3),
        Text(
          text,
          style: const TextStyle(fontSize: 18),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    ),
  );
}
