import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget headerWrapperWidget(BuildContext context,String icon,String text) {
  return Container(
    color: Colors.white,
    width: MediaQuery.of(context).size.width,
    child: Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(icon,width: 60),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 10),
              child:  Text(text,style: const TextStyle(fontSize: 24),
              ),
            ),
            const SizedBox(height: 5,)
          ],
        ),
      ],
    ),
  );
}
