import 'package:flutter/material.dart';

Widget notifitcationBox({
  required String onvan,
  required BuildContext context,
  required String matn,
  required String date,
  required String icon,
   file=null,
   file_2=null,
}) {
  return Container(
    margin: const EdgeInsets.only(bottom: 10),
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    color: Colors.white,
    width: MediaQuery.of(context).size.width,
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(
                  icon,
                  width: 20,
                ),
                const SizedBox(
                  width: 7,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width * 4 / 8,
                  child: Text(
                    onvan,
                    style: const TextStyle(color: Colors.black87, fontSize: 14),
                  ),
                ),
              ],
            ),
            Row(children: [
              Text(
                date,
                style: const TextStyle(color: Colors.grey, fontSize: 10),
              ),
              const SizedBox(
                width: 7,
              ),
              Image.asset(
                'assets/icons/calender_icon.png',
                width: 30,
              ),
            ]),
          ],
        ),
        const SizedBox(
          height: 14,
        ),
        file != null
            ? Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Image.network(file),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              )
            : Text(''),
        Text(
            textAlign: TextAlign.justify,
            matn,
            style: const TextStyle(color: Colors.black54)),
      ],
    ),
  );
}

Widget answerBox({
  required BuildContext context,
  required String matn,
  String ?file_2,
}) {
  return Container(
    margin: const EdgeInsets.only(bottom: 10),
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    color: Colors.white,
    width: MediaQuery.of(context).size.width,
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(
                  'assets/icons/answer.png',
                  width: 20,
                ),
                const SizedBox(
                  width: 7,
                ),
                Text(
                  'پاسخ اساتید',
                  style: TextStyle(color: Colors.black87),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 14,
        ),
        file_2 != null
            ? Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Image.network(file_2),
                  SizedBox(
                    height: 20,
                  ),
                ],
              )
            : Text(''),
        Text(
            textAlign: TextAlign.justify,
            matn,
            style: TextStyle(color: Colors.black54)),
      ],
    ),
  );
}
