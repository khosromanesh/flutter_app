import 'package:flutter/material.dart';

class ImagePopup extends StatefulWidget {
  final String imageUrl;

  const ImagePopup({super.key, required this.imageUrl});

  @override
  _ImagePopupState createState() => _ImagePopupState();
}

class _ImagePopupState extends State<ImagePopup> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Image.network(widget.imageUrl),
          ),
        ],
      ),
    );
  }
}
