import 'package:flutter/material.dart';

Container inputWidget(
    {required BuildContext context, required String placeHolder, required TextEditingController controller,
      int maxLines = 1,bool isSend=false}) {

  return Container(
    width: MediaQuery
        .of(context)
        .size
        .width,
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: const [
          BoxShadow(
              color: Colors.black12,
              blurRadius: 2.0,
              spreadRadius: 0.4,
              offset: Offset(0, 3))
        ]),
    child: TextField(
      controller: controller,
      maxLines: maxLines,
      decoration: InputDecoration(
        errorText: controller.text == '' && isSend ? '$placeHolder را تکیمل نمایید' : null,
        border: InputBorder.none,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),
        hintText: placeHolder,
        hintStyle: const TextStyle(fontSize: 14),
      ),
    ),
  );
}

