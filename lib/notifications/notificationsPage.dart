import 'dart:convert';
import 'package:flutter/material.dart';
import '../extra/emptyPage.dart';
import '../widgets/headerWidget.dart';
import '../widgets/notificationWidget.dart';
import 'package:http/http.dart' as http;

class notificationsPage extends StatefulWidget {
  const notificationsPage({Key? key}) : super(key: key);

  @override
  State<notificationsPage> createState() => _notificationsPageState();
}

class _notificationsPageState extends State<notificationsPage> {
  initState() {
    super.initState();
    _fetchUsers();
  }

  List<dynamic> _users = [];
  bool _show_lodding = true;
  bool _first_run = false;
  bool _empty = false;

  Future<void> _fetchUsers() async {
    if (!_first_run) {
      final response = await http.get(
          Uri.parse('https://application.shahinvest.ir/wp-json/notifications'));
      if (json.decode(response.body) != null) {
        final List<dynamic> data = json.decode(response.body);
        setState(() {
          _users = data;
          _show_lodding = false;
          _first_run = true;
          _empty = false;
        });
      } else {
        setState(() {
          _show_lodding = false;
          _empty = true;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return _show_lodding
        ? const Center(child: CircularProgressIndicator())
        : !_empty
            ? Column(
                children: [
                  // Add your desired widget here
                  headerWrapperWidget(
                      context, 'assets/icons/caution.png', 'اعلانات'),
                  const SizedBox(
                    height: 12,
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: _users.length,
                      itemBuilder: (BuildContext context, int index) {
                        final user = _users[index];
                        return ListTile(
                          title: notifitcationBox(
                              date: user['time_create'],
                              onvan: user['notification_title'],
                              context: context,
                              matn: user['notification_text'],
                              icon: 'assets/icons/bell-ring.png',
                              file: user['file'],
                              file_2: user['file_2']),
                        );
                      },
                    ),
                  ),
                ],
              )
            : SafeArea(
                child: Scaffold(
                  body: Center(
                    child: Column(
                      children: [
                        headerWrapperWidget(context, 'assets/icons/bell-ring.png',
                            'اعلانات'),
                        const SizedBox(
                          height: 12,
                        ),
                        const emptyPage()
                      ],
                    ),
                  ),
                ),
              );
  }
}

Widget buttonMenu(String text, String icon, context, route) {
  return Container(
    width: 140,
    margin: const EdgeInsets.symmetric(horizontal: 7),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        shadowColor: Colors.black26,
        foregroundColor: Colors.black54,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/routeName');
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(text),
          Image.asset(icon, width: 30),
        ],
      ),
    ),
  );
}
