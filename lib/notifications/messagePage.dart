import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shahinvest/notifications/topMenuWidget.dart';
import 'package:shahinvest/widgets/headerWidget.dart';
import '../widgets/notificationWidget.dart';

class messagesPage extends StatefulWidget {
  const messagesPage({Key? key}) : super(key: key);

  @override
  State<messagesPage> createState() => _messagesPageState();
}

class _messagesPageState extends State<messagesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          headerWrapperWidget(context, 'assets/icons/caution.png', 'اعلانات'),
          SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            scrollDirection: Axis.horizontal,
            child: Column(
              children: [
                topMenuWidget(),
                SizedBox(
                  height: 10,
                  width: MediaQuery.of(context).size.width,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 100,
          )
        ],
      ),
    );
  }
}

Widget buttonMenu(
    {required String text,
      required String icon,
      required BuildContext context,
      required String route}) {
  return Container(
    width: 140,
    margin: const EdgeInsets.symmetric(horizontal: 7),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        shadowColor: Colors.black26,
        foregroundColor: Colors.black54,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/$route');
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(text),
          Image.asset(icon, width: 30),
        ],
      ),
    ),
  );
}
