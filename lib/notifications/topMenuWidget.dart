import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'messagePage.dart';

class topMenuWidget extends StatefulWidget {
  const topMenuWidget({Key? key}) : super(key: key);

  @override
  State<topMenuWidget> createState() => _topMenuWidgetState();
}

class _topMenuWidgetState extends State<topMenuWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(bottom: 10),
      color: Colors.white,
      child: Row(
        children: [
          buttonMenu(icon:'assets/icons/bell-ring.png', text: 'هشدار ها',context: context,route: 'notificationsPage'),
          buttonMenu(icon:'assets/icons/message.png', text: 'پیام ها',context: context,route: 'messagePage'),
          buttonMenu(icon:'assets/icons/paper_icon.png', text: 'آزمون ها',context: context,route: 'beZoodi'),
        ],
      ),
    );
  }
}

// Widget buttonMenu(String text, String icon) {
//   return Container(
//     width: 140,
//     margin: EdgeInsets.symmetric(horizontal: 7),
//     child: ElevatedButton(
//       style: ElevatedButton.styleFrom(
//         shadowColor: Colors.black26,
//         foregroundColor: Colors.black54,
//         backgroundColor: Colors.white,
//         shape: const RoundedRectangleBorder(
//           borderRadius: BorderRadius.all(Radius.circular(50)),
//         ),
//       ),
//       onPressed: () {},
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceAround,
//         children: [
//           Text(text),
//           Image.asset(icon, width: 30),
//         ],
//       ),
//     ),
//   );
// }
