import 'package:flutter/material.dart';
import '../widgets/boxesWidget.dart';
import '../widgets/headerWidget.dart';

class homePage extends StatelessWidget {
  const homePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              headerWrapperWidget(
                  context, 'assets/icons/home_icon.png', 'خانه'),
              Container(
                padding: const EdgeInsets.only(bottom: 10),
                alignment: Alignment.center,
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                child: const Text('مدرسه اقتصاد شاهین',
                    style: TextStyle(fontSize: 16)),
              ),
              Container(
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 15,
                    offset: const Offset(0, 10), // changes position of shadow
                  )
                ]),
                child: SizedBox(
                  height: 10,
                  width: MediaQuery.of(context).size.width * 0.8,
                ),
              ),
              Container(
                color: Colors.white,
                child: SizedBox(
                  height: 12,
                  width: MediaQuery.of(context).size.width * 1,
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width * 1,
                color: Colors.white,
                child: Container(
                  color: Colors.white,
                  child: Row(children: [
                    Container(
                      color: Colors.white,
                      width: MediaQuery.of(context).size.width - 20,
                      child: Column(
                        children: [
                          LayoutBuilder(
                            builder: (BuildContext context,
                                BoxConstraints constraints) {
                              if (constraints.maxWidth < 320) {
                                return Column(
                                  children: [
                                    buildBoxes('assets/icons/clock_icon.png',
                                        'زنگ مدرسه', context, 'zangeMadrese'),
                                    buildBoxes('assets/icons/ask2_icon.png',
                                        'بانک سوالات', context, 'bankSoalat'),
                                    buildBoxes('assets/icons/owl_icon.png',
                                        'جزوه های آموزشی', context, 'beZoodi'),
                                    buildBoxes('assets/icons/exam_icon.png',
                                        'آزمون ها', context, 'beZoodi'),
                                  ],
                                );
                              } else {
                                return SizedBox(
                                  width: MediaQuery.of(context).size.width,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          buildBox(
                                              'assets/icons/clock_icon.png',
                                              'زنگ مدرسه',
                                              context,
                                              'zangeMadrese'),
                                          buildBox(
                                              'assets/icons/ask2_icon.png',
                                              'بانک سوالات',
                                              context,
                                              'bankSoalat'),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          buildBox(
                                              'assets/icons/owl_icon.png',
                                              'جزوه های آموزشی',
                                              context,
                                              'beZoodi'),
                                          buildBox('assets/icons/exam_icon.png',
                                              'آزمون ها', context, 'beZoodi'),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                    ],
                                  ),
                                );
                              }
                            },
                          )
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

quitApplication(BuildContext context) {
  return TextButton(
    onPressed: () => showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text('خروج'),
        content: const Text('آیا از خروج اطمینان دارید ؟'),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('لغو'),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context, 'OK'),
            child: const Text('تایید'),
          ),
        ],
      ),
    ),
    child: const Text('Show Dialog'),
  );
}
