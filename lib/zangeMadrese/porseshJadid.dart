import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:elegant_notification/elegant_notification.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../secureStorage/secureStorage.dart';
import '../widgets/headerWidget.dart';
import '../widgets/inputWidgets.dart';

class porseshJadid extends StatefulWidget {
  const porseshJadid({Key? key}) : super(key: key);

  @override
  State<porseshJadid> createState() => _porseshJadidState();
}

class _porseshJadidState extends State<porseshJadid> {
  @override
  void initState() {
    super.initState();
  }

  final onvanPorseshController = TextEditingController();
  final matnPorseshController = TextEditingController();
  bool _isSendOnvanPorsesh = false;
  bool _isSendMatnPorsesh = false;
  bool _showSendBtn = true;
  bool _isSending = false;
  late PlatformFile _file;
  late bool _file_is_selected = false;

  final List<String> dasteBandiItems = [
    'ترم یک',
    'ترم دو',
    'سایر',
  ];
  final List<String> hoozeSoal = [
    'بازار ها و نهاد های مالی',
    'پول و سیاست های پولی و مالی',
    'اقتصاد کاربردی',
    'داده های اقتصادی',
    'سایر',
  ];

  Future<void> retrieveAndSendData({onvanSoal, matnPorsesh}) async {
    // Retrieve the stored value using the key
    String? username = await storage.read(key: 'user_nicename');
    String? token = await storage.read(key: 'token');

    setState(() {
      _isSending = true;
      _showSendBtn = false;
    });
    // Send the payload in a POST request to the API
    if (false == _file_is_selected) {
      http.Response response = await http.post(
        Uri.parse('https://application.shahinvest.ir/wp-json/send_sms_verification'),
        body: {
          'onvanSoal': onvanSoal!,
          'hoozeSoalValue': hoozeSoalValue!,
          'dasteBandiValue': dasteBandiValue!,
          'matnPorsesh': matnPorsesh!,
          'username': username!,
          'token': token!,
        },
      );
      print(response.body);
      request_finished(response);
    } else {
      // file selected
      print('file selected');
      var request = http.MultipartRequest('POST',
          Uri.parse('https://application.shahinvest.ir/wp-json/ask_question'));
      request.files.add(await http.MultipartFile.fromPath('file', _file.path!));
      request.fields['onvanSoal'] = onvanSoal;
      request.fields['hoozeSoalValue'] = hoozeSoalValue!;
      request.fields['dasteBandiValue'] = dasteBandiValue!;
      request.fields['matnPorsesh'] = matnPorsesh;
      request.fields['username'] = username!;
      request.fields['token'] = token!;
      var response = await request.send();
      // http.Response response = await http.post(
      //   Uri.parse('https://application.shahinvest.ir/wp-json/ask_question'),
      //   body: {
      //     'onvanSoal': onvanSoal,
      //     'hoozeSoalValue': hoozeSoalValue,
      //     'dasteBandiValue': dasteBandiValue,
      //     'matnPorsesh': matnPorsesh,
      //     'username': value,
      //     'token': token,
      //   },
      // );
      request_finished(response);
    }

    // Check the response status code
  }

  String? dasteBandiValue;
  String? hoozeSoalValue;
  final _formKey = GlobalKey<FormState>();

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                headerWrapperWidget(
                    context, 'assets/icons/ask_icon.png', 'پرسش جدید'),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        Form(
                          key: _formKey,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                inputWidget(
                                    context: context,
                                    controller: onvanPorseshController,
                                    placeHolder: 'عنوان پرسش',
                                    isSend: _isSendOnvanPorsesh),
                                const SizedBox(height: 20),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 2.0,
                                            spreadRadius: 0.4,
                                            offset: Offset(0, 3))
                                      ]),
                                  child: DropdownButtonFormField2(
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.zero,
                                      border: InputBorder.none,
                                    ),
                                    isExpanded: true,
                                    hint: const Text(
                                      'دسته بندی',
                                      style: TextStyle(fontSize: 14),
                                    ),
                                    items: dasteBandiItems
                                        .map((item) => DropdownMenuItem<String>(
                                              value: item,
                                              child: Text(
                                                item,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ))
                                        .toList(),
                                    validator: (value) {
                                      if (value == null) {
                                        return 'لطفا دسته بندی را انتخاب نمایید';
                                      }
                                      return null;
                                    },
                                    onChanged: (value) {
                                      setState(() {
                                        dasteBandiValue = value;
                                      });
                                    },
                                    buttonStyleData: const ButtonStyleData(
                                      height: 50,
                                      padding:
                                          EdgeInsets.only(left: 20, right: 10),
                                    ),
                                    iconStyleData: const IconStyleData(
                                      icon: Icon(
                                        Icons.arrow_drop_down,
                                        color: Colors.black45,
                                      ),
                                      iconSize: 30,
                                    ),
                                    dropdownStyleData: const DropdownStyleData(
                                        decoration: BoxDecoration(boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 2.0,
                                          spreadRadius: 0.4,
                                          offset: Offset(0, 3))
                                    ])),
                                  ),
                                ),
                                const SizedBox(height: 20),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          new BorderRadius.circular(10.0),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 2.0,
                                            spreadRadius: 0.4,
                                            offset: Offset(0, 3))
                                      ]),
                                  child: DropdownButtonFormField2(
                                    decoration: const InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.zero,
                                      border: InputBorder.none,
                                    ),
                                    isExpanded: true,
                                    hint: const Text(
                                      'حوزه سوال',
                                      style: TextStyle(fontSize: 14),
                                    ),
                                    items: hoozeSoal
                                        .map((item) => DropdownMenuItem<String>(
                                              value: item,
                                              child: Text(
                                                item,
                                                style: const TextStyle(
                                                  fontSize: 14,
                                                ),
                                              ),
                                            ))
                                        .toList(),
                                    validator: (value) {
                                      if (value == null) {
                                        return 'لطفا حوزه سوال را انتخاب نمایید';
                                      }
                                      return null;
                                    },
                                    onChanged: (value) {
                                      setState(() {
                                        hoozeSoalValue = value;
                                      });
                                    },
                                    buttonStyleData: const ButtonStyleData(
                                      height: 50,
                                      padding:
                                          EdgeInsets.only(left: 20, right: 10),
                                    ),
                                    iconStyleData: const IconStyleData(
                                      icon: Icon(
                                        Icons.arrow_drop_down,
                                        color: Colors.black45,
                                      ),
                                      iconSize: 30,
                                    ),
                                    dropdownStyleData: const DropdownStyleData(
                                        decoration: BoxDecoration(boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          blurRadius: 2.0,
                                          spreadRadius: 0.4,
                                          offset: Offset(0, 3))
                                    ])),
                                  ),
                                ),
                                const SizedBox(height: 20),
                                inputWidget(
                                  placeHolder: 'متن پرسش',
                                  controller: matnPorseshController,
                                  context: context,
                                  maxLines: 7,
                                  isSend: _isSendMatnPorsesh,
                                ),
                                const SizedBox(height: 30),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width,
                                  child: ElevatedButton(
                                      onPressed: () async {
                                        FilePickerResult? result =
                                            await FilePicker.platform.pickFiles(
                                          type: FileType.custom,
                                          allowedExtensions: [
                                            'jpg',
                                            'jpeg',
                                            'png',
                                            'tiff',
                                          ],
                                        );
                                        if (result != null) {
                                          _file = result.files.first;
                                          setState(() {
                                            _file_is_selected = true;
                                          });
                                        } else {
                                          setState(() {
                                            _file_is_selected = false;
                                          });
                                        }
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        )),
                                        backgroundColor: MaterialStateProperty
                                            .resolveWith<Color?>(
                                          (Set<MaterialState> states) {
                                            return Colors
                                                .white; // defer to the defaults
                                          },
                                        ),
                                        foregroundColor: MaterialStateProperty
                                            .resolveWith<Color?>(
                                          (Set<MaterialState> states) {
                                            return Colors
                                                .black54; // defer to the defaults
                                          },
                                        ),
                                      ),
                                      child: const Text('انتخاب فایل')),
                                ),
                                const SizedBox(height: 30),
                                !_isSending
                                    ? Visibility(
                                        visible: !_isSending,
                                        child: TextButton(
                                          onPressed: () {
                                            setState(() {
                                              _isSendMatnPorsesh = true;
                                              _isSendOnvanPorsesh = true;
                                            });
                                            final FormState? formState =
                                                _formKey.currentState;
                                            if (formState != null &&
                                                formState.validate()) {
                                              formState.save();
                                              if (onvanPorseshController.text !=
                                                      '' &&
                                                  matnPorseshController.text !=
                                                      '') {
                                                retrieveAndSendData(
                                                    onvanSoal:
                                                        onvanPorseshController
                                                            .text,
                                                    matnPorsesh:
                                                        matnPorseshController
                                                            .text);
                                              }
                                            }
                                          },
                                          child: const Text(
                                            'ارسال',
                                          ),
                                        ),
                                      )
                                    : const Text('لطفا صبر کنید'),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void request_finished(response) {
    if (response.statusCode == 200) {
      setState(() {
        _showSendBtn = true;
        _isSending = false;
        onvanPorseshController.clear();
        matnPorseshController.clear();
        successfulNotification(context);
        _file_is_selected = false;
      });
    } else {
      // Handle the error response
      _isSending = false;
    }
  }
}

// Future<void> sendPorseshRequest(
//     {onvanSoal, dasteBandiValue, hoozeSoalValue, matnPorsesh}) async {
//   final url =
//       Uri.parse('https://application.shahinvest.ir/wp-json/ask_question');
//   final storage = FlutterSecureStorage();
//   String? username = await storage.read(key: 'user_display_name');
//   final response = await http.post(
//     url,
//     headers: <String, String>{
//       'Content-Type': 'application/json; charset=UTF-8',
//     },
//     body: {
//       'onvanSoal': onvanSoal,
//       'hoozeSoalValue': hoozeSoalValue,
//       'dasteBandiValue': dasteBandiValue,
//       'matnPorsesh': matnPorsesh,
//       'username': username!,
//     },
//   );
//
//   if (response.statusCode == 200) {
//     // Handle successful response
//     // print('Response body: ${response}');
//   } else {
//     // Handle error response
//     print('Error: ${response.reasonPhrase}');
//   }
// }

void successfulNotification(context) {
  return ElegantNotification.success(
          title: const Text("موفق"), description: const Text("عملیات موفقیت آمیز بود"))
      .show(context);
}
