import 'package:flutter/material.dart';
import 'package:shahinvest/widgets/boxesWidget.dart';
import '../widgets/headerWidget.dart';

class zangeMadrese extends StatelessWidget {
  const zangeMadrese({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Colors.transparent,
          child: Column(
            children: [
              headerWrapperWidget(
                  context, 'assets/icons/clock_icon.png', 'زنگ مدرسه'),
              const SizedBox(height: 10,),
              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Column(
                  children: [
                    buildBox('assets/icons/ask_icon.png', 'پرسش جدید', context, 'porseshJadid'),
                    buildBox('assets/icons/tahlil.png', 'تحلیل جدید', context, 'beZoodi')
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
