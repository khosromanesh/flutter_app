import 'package:flutter_secure_storage/flutter_secure_storage.dart';

final storage = FlutterSecureStorage();

Future<String?> getUserNicename() async {
  return await storage.read(key: 'user_nicename');
}
Future<String?> getUsername() async {
  return await storage.read(key: 'user_display_name');
}


