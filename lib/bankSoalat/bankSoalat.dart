import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shahinvest/widgets/boxesWidget.dart';
import '../widgets/headerWidget.dart';

class bankSoalat extends StatelessWidget {
  const bankSoalat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                headerWrapperWidget(
                    context, 'assets/icons/ask2_icon.png', 'بانک سوالات'),
                SizedBox(height: 10,),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Column(
                    children: [
                      buildBox('assets/icons/mine.png', 'پرسش های من', context, 'porseshHayeMan'),
                      buildBox('assets/icons/check.png', 'پرسش های منتخب', context, 'porseshHayeMontekhab'),
                      buildBox('assets/icons/analysis.png', 'تحلیل های منتخب', context, 'beZoodi')
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
