import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shahinvest/bankSoalat/pasokhPage.dart';
import 'package:shahinvest/extra/emptyPage.dart';
import '../extra/functions.dart';
import '../secureStorage/secureStorage.dart';
import '../widgets/headerWidget.dart';
import '../widgets/notificationWidget.dart';
import 'package:http/http.dart' as http;

class porseshHayeMan extends StatefulWidget {
  const porseshHayeMan({Key? key}) : super(key: key);

  @override
  State<porseshHayeMan> createState() => _porseshHayeManState();
}

class _porseshHayeManState extends State<porseshHayeMan> {
  initState() {
    super.initState();
    _fetchQuestions();
  }

  List<dynamic> _questions = [];
  String _username = '';
  bool _show_lodding = true;
  bool _showLading = true;
  bool _first_run = false;
  bool _isHeaderShow = false;
  bool _empty = false;

  Future<void> _fetchQuestions() async {
    if (!_first_run) {
      String? token = await storage.read(key: 'token');
      String? value = await storage.read(key: 'user_nicename');
      // Create a JSON payload with the stored value
      Map<String, dynamic> payload = {'user_display_name': value};

      final response = await http.post(
          Uri.parse('https://application.shahinvest.ir/wp-json/myquestions'),
          body: {'username': value, 'token': token});
      if (json.decode(response.body) != null) {
        print('not null');
        print(response.body);
        final List<dynamic> data = json.decode(response.body);
        setState(() {
          _questions = data;
          _show_lodding = false;
          _first_run = true;
        });
      } else {
        setState(() {
          _show_lodding = false;
          _empty = true;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return _show_lodding
        ? SafeArea(
            child: Scaffold(body: Center(child: CircularProgressIndicator())))
        : !_empty ? SafeArea(
            child: Scaffold(
              body: Column(
                children: [
                  // Add your desired widget here
                  headerWrapperWidget(
                      context, 'assets/icons/mine.png', 'پرسش های من'),
                  SizedBox(
                    height: 12,
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: _questions.length,
                      itemBuilder: (BuildContext context, int index) {
                        final question = _questions[index];
                        return GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => pasokhPage(
                                    context: context,
                                    matnPorsesh: question['matnPorsesh'],
                                    onvanSoal: question['onvanSoal'],
                                    hoozeSoalValue: question['hoozeSoalValue'],
                                    dasteBandiValue: question['dasteBandiValue'],
                                    video: question['video'],
                                    sound: question['sound'],
                                    file: question['file'] != null
                                        ? question['file']
                                        : null,
                                    pasokh: question['pasokh'],
                                    time_create: question['time_create'],
                                    file_2: question['file_2'] != null
                                        ? question['file_2']
                                        : null),
                              ),
                            );
                          },
                          child: notifitcationBox(
                              date: question['time_create'],
                              onvan: question['onvanSoal'],
                              context: context,
                              matn: truncateString(question['matnPorsesh']),
                              icon: 'assets/icons/mine.png',
                              file: null,
                              file_2: null),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ) : SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            children: [
              headerWrapperWidget(
                  context, 'assets/icons/mine.png', 'پرسش های من'),
              SizedBox(
                height: 12,
              ),
              emptyPage()
            ],
          ),
        ),
      ),
    );
  }
}

Widget buttonMenu(String text, String icon, context, route) {
  return Container(
    width: 140,
    margin: EdgeInsets.symmetric(horizontal: 7),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        shadowColor: Colors.black26,
        foregroundColor: Colors.black54,
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
      ),
      onPressed: () {
        Navigator.pushNamed(context, '/routeName');
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(text),
          Image.asset(icon, width: 30),
        ],
      ),
    ),
  );
}
