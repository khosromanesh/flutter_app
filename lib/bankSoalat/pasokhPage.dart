import 'package:flutter/material.dart';
import 'package:shahinvest/widgets/headerWidget.dart';
import '../widgets/notificationWidget.dart';
import '../widgets/videoPlayerWidget.dart';

SafeArea pasokhPage(
    {required String matnPorsesh,
    required BuildContext context,
    required dasteBandiValue,
    required hoozeSoalValue,
    required time_create,
    required onvanSoal,
    required pasokh,
    file,
    file_2,
    required video,
    required sound}) {
  return SafeArea(
    child: Scaffold(
        body: SingleChildScrollView(
      child: SafeArea(
          child: Column(
        children: [
          headerWrapperWidget(context, 'assets/icons/question.png', 'سوال'),
          SizedBox(
            height: 10,
          ),
          notifitcationBox(
              date: time_create,
              onvan: onvanSoal,
              context: context,
              file: file,
              file_2: file_2,
              matn: matnPorsesh,
              icon: 'assets/icons/question.png'),
          answerBox(context: context, matn: pasokh, file_2: file_2),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              sound != null
                  ? TextButton(
                      onPressed: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VideoPlayerWidget(
                                        videoUrl: sound,
                                      )),
                            )
                          },
                      child: Text('فایل صوتی'))
                  : Text(''),
              video != null
                  ? TextButton(
                      onPressed: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VideoPlayerWidget(
                                        videoUrl: video,
                                      )),
                            )
                          },
                      child: Text('فایل تصویری'))
                  : Text(''),
            ],
          ),
        ],
      )),
    )),
  );
}
