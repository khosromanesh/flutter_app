import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:floating_frosted_bottom_bar/floating_frosted_bottom_bar.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shahinvest/login/loginPageOtp.dart';
import 'package:shahinvest/notifications/messagePage.dart';
import 'package:shahinvest/zangeMadrese/porseshJadid.dart';
import 'package:shahinvest/zangeMadrese/zangeMadrese.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'bankSoalat/bankSoalat.dart';
import 'bankSoalat/porseshHayeMan.dart';
import 'bankSoalat/porseshHayeMontekhab.dart';
import 'beZoodi/beZoodi.dart';
import 'extra/exit-popup.dart';
import 'home/homePage.dart';
import 'login/loginPage.dart';
import 'notifications/notificationsPage.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        localizationsDelegates: const [
          GlobalCupertinoLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: const [
          Locale("fa", "IR"),
        ],
        locale: Locale("fa", "IR"),
        color: Colors.white,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: 'vazir_matn',
          primarySwatch: Colors.purple,
        ),
        routes: routes,
        home: FutureBuilder<String?>(
          future: FlutterSecureStorage().read(key: 'token'),
          builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
            if (!snapshot.hasData && snapshot.data == null) {
              return SafeArea(child: LoginPage());
            } else {
              return WillPopScope(
                  onWillPop: () => showExitPopup(context),
                  child: const MyHomePage());
            }
          },
        ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late int currentPage;
  late TabController tabController;
  bool _newNotification = false;

  final List<Color> colors = [
    Colors.purpleAccent,
    Colors.purpleAccent,
    Colors.purpleAccent,
  ];

  @override
  void initState() {
    _checkNotification();
    currentPage = 0;
    tabController = TabController(length: 2, vsync: this);
    tabController.animation!.addListener(
      () {
        final value = tabController.animation!.value.round();
        if (value != currentPage && mounted) {
          changePage(value);
        }
      },
    );
    super.initState();
  }

  Future<void> _checkNotification() async {

    final response = await http.get(
        Uri.parse('https://application.shahinvest.ir/wp-json/notifications'));
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // late final String? lastNotificationId = prefs.getString('hossein');
    // print(lastNotificationId);

    // if (lastNotificationId == jsonDecode(response.body).last['id']) {
    //   setState(() {
    //     _newNotification = false;
    //   });
    // } else {
    //   await prefs.remove('last_notification_id');
    //   await prefs.setString(
    //       'last_notification_id', jsonDecode(response.body).last['id']);
    //   setState(() {
    //     _newNotification = true;
    //   });
    // }
  }

  void changePage(int newPage) {
    setState(() {
      currentPage = newPage;
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: FrostedBottomBar(
          opacity: 0.6,
          sigmaX: 5,
          sigmaY: 5,
          borderRadius: BorderRadius.circular(500),
          duration: const Duration(milliseconds: 800),
          hideOnScroll: true,
          body: (context, controller) => TabBarView(
            controller: tabController,
            dragStartBehavior: DragStartBehavior.down,
            physics: NeverScrollableScrollPhysics(),
            children: [
              homePage(),
              notificationsPage(),
            ],
          ),
          child: TabBar(
            indicatorPadding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
            controller: tabController,
            indicator: const UnderlineTabIndicator(
              borderSide: BorderSide(color: Colors.purpleAccent, width: 4),
              insets: EdgeInsets.fromLTRB(16, 0, 16, 8),
            ),
            tabs: [
              TabsIcon(
                  icons: Icons.home,
                  color: currentPage == 0 ? colors[0] : Colors.white),
              TabsIcon(
                  icons: _newNotification
                      ? Icons.notifications_active
                      : Icons.notifications,
                  color: currentPage == 1 ? colors[1] : Colors.white),
              // TabsIcon(
              //     icons: Icons.person,
              //     color: currentPage == 2 ? colors[2] : Colors.white),
            ],
          ),
        ),
      ),
    );
  }
}

class TabsIcon extends StatelessWidget {
  final Color color;
  final double height;
  final double width;
  final IconData icons;

  const TabsIcon(
      {Key? key,
      this.color = Colors.white,
      this.height = 60,
      this.width = 50,
      required this.icons})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: Center(
        child: Icon(
          icons,
          color: color,
        ),
      ),
    );
  }
}

final Map<String, WidgetBuilder> routes = {
  '/zangeMadrese': (context) => const zangeMadrese(),
  '/bankSoalat': (context) => const bankSoalat(),
  '/porseshJadid': (context) => const porseshJadid(),
  '/tahlilJadid': (context) => const beZoodi(),
  '/beZoodi': (context) => const beZoodi(),
  '/notificationsPage': (context) => const notificationsPage(),
  '/messagePage': (context) => const messagesPage(),
  '/porseshHayeMan': (context) => const porseshHayeMan(),
  '/porseshHayeMontekhab': (context) => const porseshHayeMontekhab(),
  '/loginPageOtp': (context) => const LoginPageOtp(),
  '/loginPage': (context) => const LoginPage(),
  '/homePage': (context) => const MyHomePage(),
};
