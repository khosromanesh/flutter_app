import 'package:flutter/material.dart';
import '../widgets/headerWidget.dart';

class beZoodi extends StatelessWidget {
  const beZoodi({Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                headerWrapperWidget(
                    context, 'assets/icons/wait.png', 'به زودی'),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Image.asset('assets/icons/programming.png', width: MediaQuery.of(context).size.width-100,),
                        Text('تیم برنامه نویسی در حال توسعه نرم افزار می باشد و به زودی آپشن های بیشتری در اختیار شما عزیزان قرار خواهد گرفت.'),
                        const RotationTransition(
                          turns:  AlwaysStoppedAnimation(90 / 360),
                          child:  Text("( :",style: TextStyle(fontSize: 24),),
                        ),SizedBox(height: 50,)
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
