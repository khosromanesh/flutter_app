import 'package:flutter/material.dart';

class emptyPage extends StatelessWidget {
  const emptyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.center,
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Image.asset('assets/icons/notFound.png'),
          Text('متحوایی وجود ندارد'),
        ],
      ),
    );
  }
}
