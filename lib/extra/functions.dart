String truncateString(String str) {
  if (str.length <= 100) {
    return str;
  } else {
    return '${str.substring(0, 200)}...';
  }
}